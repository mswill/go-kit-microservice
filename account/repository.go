package account

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/log"
	"gorm.io/gorm"
)

// Checking repo struct and Repository interface
var _ Repository = &repo{}

var RepoErr = errors.New("Unable to handle Repo Request")

type repo struct {
	DB     *gorm.DB
	logger log.Logger
}

func NewRepo(DB *gorm.DB, logger log.Logger) *repo {
	return &repo{
		DB:     DB,
		logger: log.With(logger, "repo", "sql gorm")}
}

func (r repo) CreateUser(ctx context.Context, user User) error {
	level.Info(r.logger).Log("method", "CreateUser")

	if user.Email == "" && user.Password == "" {
		return RepoErr
	}

	r.DB.Create(&user)

	return nil
}

func (r repo) GetUser(ctx context.Context, id int) (string, error) {

	tx := r.DB.Where(id).First(&User{})
	fmt.Println(r.logger.Log(tx))
	return "", nil
}
