package account

import (
	"context"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID       int    `json:"id,omitempty"`
	Email    string `json:"email"`
	Password string `json:"password"`
	City     string `json:"city"`
}

type Repository interface {
	CreateUser(ctx context.Context, user User) error
	GetUser(ctx context.Context, id int) (string, error)
}
