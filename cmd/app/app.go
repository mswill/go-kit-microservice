package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/subosito/gotenv"
	"go-kit-microservice/account"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func init() {
	err := gotenv.Load(".env")
	if err != nil {
		fmt.Println(err)
		return
	}

}
func main() {

	port, _ := strconv.Atoi(os.Getenv("port"))
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable",
		os.Getenv("host"),
		os.Getenv("user"),
		os.Getenv("password"),
		os.Getenv("dbname"),
		port)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return
	}

	// MAKE AUTO MIGRATION
	go migrate(db)

	var httpAddr = flag.String("http", ":8081", "http listen address")

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "account",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service finished")

	flag.Parse()
	ctx := context.Background()

	//implementation of dependencies
	repo := account.NewRepo(db, logger)
	srv := account.NewService(repo, logger)
	endpoints := account.MakeEndpoints(srv)

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		fmt.Println("Listnening port ", *httpAddr)
		handler := account.NewHttpServer(ctx, endpoints)
		errs <- http.ListenAndServe(*httpAddr, handler)
	}()
	level.Error(logger).Log("Exit", <-errs)
}

func migrate(db *gorm.DB) {

	fmt.Println("<----- Migration -----> ")
	err := db.AutoMigrate(&account.User{})
	if err != nil {
		fmt.Println(err)

		return
	}
}
